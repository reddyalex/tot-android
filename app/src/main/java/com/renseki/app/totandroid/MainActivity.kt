package com.renseki.app.totandroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.totandroid.view.aboutme.AboutMeActivity
import com.renseki.app.totandroid.view.playground.PlaygroundActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_play.setOnClickListener {
            val intent = PlaygroundActivity.getStartIntent(this, "Erick")
            startActivity(intent)
        }

        btn_show_me.setOnClickListener {
            startActivity(
                AboutMeActivity.getStartIntent(this)
            )
        }
    }
}
