package com.renseki.app.totandroid
import  android.app.Application

class MyApp : Application() {

    companion object {
        const val SHARED_PREF_FILE_NAME = "settings"
    }
}