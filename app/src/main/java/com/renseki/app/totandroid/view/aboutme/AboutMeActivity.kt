package com.renseki.app.totandroid.view.aboutme

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.totandroid.MyApp
import com.renseki.app.totandroid.R
import com.renseki.app.totandroid.commons.SingleInputActivity
import com.renseki.app.totandroid.databinding.AboutMeActivityBinding
import com.renseki.app.totandroid.model.Me
import kotlinx.android.synthetic.main.about_me_activity.*

class AboutMeActivity : AppCompatActivity() {

    companion object {
        private const val SINGLE_INPUT_CODE = 888
        fun getStartIntent(context: Context)
                = Intent(context, AboutMeActivity::class.java)
    }
    private lateinit var me: Me
    private lateinit var binding : AboutMeActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
        R.layout.about_me_activity)

        me = Me(
            getPresistedName("Reddy Alexandro"),
            "https://pbs.twimg.com/profile_images/3700805240/1b5e372301990dcebaa114522745982c.jpeg",
            "erick@stts.edu",
            "0812345678"
        )

        setupMyInfo()
        setupEvents()
    }

    private fun setupEvents() {
        tv_phone.setOnClickListener {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:${me.phone}")
            startActivity(dialIntent)
        }

        tv_location.setOnClickListener {
            val locationIntent = Intent(Intent.ACTION_VIEW)
            locationIntent.data = Uri.parse("geo:-7.291306,112.7566403")
            startActivity(locationIntent)
        }
        tv_name.setOnClickListener{
                startActivityForResult(
                    SingleInputActivity.getStartIntent(this,me.name),
                    SINGLE_INPUT_CODE
                )

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            SINGLE_INPUT_CODE -> {
                if(resultCode == Activity.RESULT_OK){
                    val newValue = data?.getStringExtra(SingleInputActivity.DATA_VALUE)?: ""
                    me = me.copy(
                        name= newValue
                    )
                    setupMyInfo()
                    presistName()

                }

            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }

    }

    private fun presistName(){
        val sharedPref=getSharedPreferences(
            MyApp.SHARED_PREF_FILE_NAME
            , Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(getString(R.string.key_my_name),me.name)
        editor.apply()
    }

    private fun getPresistedName(defaultName:String):String{
        val sharedPref = getSharedPreferences(MyApp.SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE)
        return sharedPref.getString(getString(R.string.key_my_name),defaultName)?: defaultName
    }

    private fun setupMyInfo() {


        binding.me =  me
        /*tv_email.text = me.email
        tv_phone.text = me.phone*/
        tv_location.text = "Ngagel Jaya Tengah 73-77"
    }
}
