package com.renseki.app.totandroid.view.playground

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.totandroid.R
import com.renseki.app.totandroid.util.extension.toast

class PlaygroundActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_NAME = "EXTRA_NAME"

        fun getStartIntent(context: Context, name: String)
            = Intent(context, PlaygroundActivity::class.java).apply {
            putExtra(EXTRA_NAME, name)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playground)

        val myName = intent.getStringExtra(EXTRA_NAME)
        toast(myName)
    }
}