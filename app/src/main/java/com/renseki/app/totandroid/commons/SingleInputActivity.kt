package com.renseki.app.totandroid.commons

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.renseki.app.totandroid.R
import kotlinx.android.synthetic.main.single_input_activity.*

class SingleInputActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_VALUE = "EXTRA_VALUE"
        const val DATA_VALUE = "DATA_VALUE"
        fun getStartIntent(context: Context, value: String): Intent {
            val intent = Intent(context, SingleInputActivity::class.java)
            intent.putExtra(EXTRA_VALUE, value)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.single_input_activity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val value = intent.getStringExtra(EXTRA_VALUE)
        ed_value.setText(value)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_single_input, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            R.id.menu_item_submit -> {
                returnEditTextValue()
                true
            }
            android.R.id.home ->{
                finish()
                true
            }
            else ->{
                return super.onOptionsItemSelected(item)
            }
        }
    }

    private fun returnEditTextValue() {

        val newValue = ed_value.text.toString()
        val intent = Intent()
        intent.putExtra(DATA_VALUE, newValue)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
